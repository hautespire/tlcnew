//// js/MainController.js

app.controller('MainController', ['$scope', function($scope) {
	
	$scope.socialSites = 
	[
		{
			name: 'Twitter',
			//icon: 'img/social/twitter.png',
			icon: 'img/social/site-stub.png',
			url: 'http://www.twitter.com/coldspire'	
		},
		{
			name: 'LinkedIn',
			//icon: 'img/social/linkedin.png',
			icon: 'img/social/site-stub.png',
			url: 'https://www.linkedin.com/in/owenra'
		},
		{
			name: 'Bitbucket',
			//icon: 'img/social/bitbucket.png',
			icon: 'img/social/site-stub.png',
			url: 'https://bitbucket.org/hautespire/'
		},
		{
			name: 'Github',
			//icon: 'img/social/github.png',
			icon: 'img/social/site-stub.png',
			url: 'http://www.github.com/coldspire/'	
		},
		{
			name: 'Flickr',
			//icon: 'img/social/flickr.png',
			icon: 'img/social/site-stub.png',
			url: 'https://www.flickr.com/photos/coldspire'
		}
	];
}]);