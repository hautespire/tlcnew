/* js/directives/socialLink.js */

app.directive('socialLink', function() {
	return {
		restrict: 'E',
		scope: { site: '=' },
		templateUrl: 'js/directives/socialLink.html'
	};
});